<div class="sticky-top-placeholder">
	<div class="menu-container">
		<div class="menu-item menu-logo">
		</div>
		<div style="display:none;" class="menu-item menu-title">
			Alton&Bit Co.
		</div>
		<div class="menu-list-wrapper">
			<div style="float:left;">
				<div class="menu-item menu-tab menu-linked-item" onclick="scollToId(this, 'product-section')">
					<?php if($lang === 'ko') { ?>
					 &nbsp;제품 &nbsp;
					<?php } else { ?>
					PRODUCTS
					<?php } ?>
				</div>
				<div class="menu-item menu-tab menu-linked-item" onclick="scollToId(this, 'application-section')">
					<?php if($lang === 'ko') { ?>
					 &nbsp;적용분야 &nbsp;
					<?php } else { ?>
					APPLICATIONS
					<?php } ?>
				</div>
				<div class="menu-item menu-tab menu-linked-item" onclick="scollToId(this, 'company-section')">
					<?php if($lang === 'ko') { ?>
					 &nbsp;회사소개&nbsp; 
					<?php } else { ?>
					COMPANY
					<?php } ?>
				</div>
			</div>
			<div class="menu-item menu-tab">
				<select class="menu-select vertical-center" id="lang_option" name="lang" onchange='onLangChanged()'>
					<option value="ko" <?php if($lang === 'ko') echo 'selected'?>>한국어</option>
					<option value="en" <?php if($lang === 'en') echo 'selected'?>>English</option>
				</select>
			</div>
		</div>
	</div>
</div>