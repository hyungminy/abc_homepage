<?php
ob_start();
$file_name = basename(__FILE__);

include 'config.php';
?>

<html lang="<?php if($lang === 'ko') echo 'ko-ko'; else echo 'en'; ?>" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta name="description" content="Atom&Bit Co.">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>ABC</title>
	<link rel="icon" href="images/icon.png">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	
	<link type="text/css" rel="stylesheet" href="css/styles.css"/>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src = "https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>

	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	
	<script src="js/common.js"></script>
	<script src="js/index.js"></script>
	<script>

	function scollToId(obj, id) {
		document.getElementById(id + "-anchor").scrollIntoView({ behavior: 'smooth', block: 'start' });

		$(obj).siblings().removeClass("menu-selected");
		$(obj).addClass("menu-selected");
	}
	function onScroll() {
		var list = document.getElementsByClassName("menu-linked-section");
		var menuList = document.getElementsByClassName("menu-linked-item");
		for(var i = 0 ; i < list.length ; i++) {
			if(isScrolledIntoView(list[i])) {
				if(!menuList[i].classList.contains("menu-selected"))
					menuList[i].classList.add("menu-selected");
			} else {
				if(menuList[i].classList.contains("menu-selected"))
					menuList[i].classList.remove("menu-selected");
			}
		}
	}
	function isScrolledIntoView(el)
	{
	    var rect = el.getBoundingClientRect();
	    var elemTop = rect.top;
	    var elemBottom = rect.bottom;

	    if(elemTop <= 0 && elemBottom > window.innerHeight * 0.6) {
	    	return true;
	    } else if(elemTop > 0 && elemTop <= window.innerHeight*0.6) {
	    	return true;
	    } else if(elemTop >= 0 && elemBottom <= window.innerHeight) {
	    	return true;
	    }
	    return false;
	}
	</script>
</head>
<body onscroll="onScroll()">
<?php
	include 'header.php';
?>
<div class="section-with-image child-vertical-middle">
	<div class="darken">
	</div>
	<div class="section-text">
	<h1>
		<?php if($lang === 'ko') { ?>
		디지털로 증강된 아날로그 세계,</br class="br-on-mobile"> 증강현실
		<?php } else { ?>
		Augmented Reality.</br class="br-on-mobile"> The real world fortified by digital technology.
		<?php } ?>
	</h1>
	<h1>
		<?php if($lang === 'ko') { ?>
		산업, 상업, 엔터테인먼트</br class="br-on-mobile"> AR의 선도기업,</br class="br-on-mobile"> 아톰앤비트
		<?php } else { ?>
		Atom&Bit, a Global leader in industry, commerce and entertainment AR
		<?php } ?>

	</h1>
	<p>
		<?php if($lang === 'ko') { ?>
		손 끝에 펼쳐지는 강력하고</br class="br-on-mobile"> 손쉬운 디지털 가이드
		<?php } else { ?>
		Powerful and intuitive guidance at your finger tip
		<?php } ?>
	</p>
	</div>
</div>
<div class="second-section">
	<p class="second-section-text">
		<span class="second-section-text-title">
			<?php if($lang === 'ko') echo '기업용 AR solution을 소개합니다'; else echo 'INTRODUCTION ABC AR FOR THE ENTERPRISE';?></span><br><?php if($lang === 'ko') echo '이제 쉽게 강력하고 직관적인 원격소통을 하세요.'; else echo "POWERFUL AND INTUITIVE REMOTE GUIDANCE IS NOW AT YOUR TEAM'S FINGERTIPS.";?>
	</p>
</div>

<div class="section-product text-center menu-linked-section" id="product-section">
	<div id="product-section-anchor" class="anchor-element"></div>
	<p class="section-product-title"><?php if($lang === 'ko') echo '제품'; else echo 'PRODUCTS';?></p>

	<h2>WorkAR&trade;<?php if($lang === 'ko'){} else {echo ' is the ultimate solution for Industry AR and Commerce AR.';}?></h2>
	<?php if($lang === 'ko') {
	?>
	<p>산업용 AR과 상업용 AR을 위한 최고의 솔루션<br>WorkAR&trade; 제품군: MappAR, ReportAR, LocatAR, TalkAR, TeachAR</p>
	<?php } else {
	?>
	<p>WorkAR&trade; consists of MappAR&trade;, ReportAR&trade;, LocatAR&trade;, TalkAR&trade; and TeachAR&trade;.</p>
	<?php } ?>
	<div class="container">
		<div class="row">
			<div class="col-sm-4 padding-top-med">
				<h3><img src="images/product/map.svg"/> MappAR&trade; <?php if($lang === 'ko') { }  else { echo 'introduction';} ?></h3>
				<p>
					<?php if($lang === 'ko') { ?>
					모든 사업/상업용 AR 애플리케이션을 위한 강력한 3D+ 모델링 솔루션. 우리의 3D+ 솔루션은 일반적인 3D 환경뿐만 아니라 다양한 응용에 맞춘 추가적 모델링을 제공합니다.
					<?php } else { ?>
					Powerful 3D+ modeling solution for any industry or commerce AR applications. We distinguish ourselves by adding exciting new dimensions to the ordinary 3D environment.
					<?php } ?>
				</p>
			</div>
			<div class="col-sm-4 padding-top-med">
				<h3><img src="images/product/report.svg"/> ReportAR&trade;</h3>
				<p>
					<?php if($lang === 'ko') { ?>
					현장에서 유용한 정보를 직관적으로 실시간 시각화하여 작업효율을 극적으로 개선합니다. 오류 및 오작동에 대해 즉각적인 조치가 가능하며 원격 모니터링 및 지원을 제공할 수 있습니다.
					<?php } else { ?>
					Intuitive real-time visualization of information on site and easy-to-follow visual guidance. Immediate action in case of error or malfunction. Remote monitoring and assistance.
					<?php } ?>
				</p>
			</div>
			<div class="col-sm-4 padding-top-med">
				<h3><img src="images/product/locat.svg"/> LocatAR&trade;</h3>
				<p>
					<?php if($lang === 'ko') { ?>
					장치 및 사람 추적을 위한 정확한 시각적 위치 확인 솔루션. 이동 / 궤적 관리 및 프로세스 최적화에 매우 적합합니다.
					<?php } else { ?>
					Accurate visual positioning of people and devices. Excellent trajectory management and process optimization.
					<?php } ?>
				</p>
			</div>
			<div class="col-sm-8 col-centered">
				<div class="row"> 
					<div class="col-sm-6 padding-top-med">
						<h3><img src="images/product/talk.svg"/> TalkAR&trade;</h3>
						<p>
							<?php if($lang === 'ko') { ?>
							원격 공동 작업자와 현장 상황을 공유하는 실용적 원격 보조 시스템. 인적 자원의 효율적인 활용이 가능합니다.
							<?php } else { ?>
							Remote assistant system for long-distance collaboration. Efficient utilization of human resources.
							<?php } ?>
						</p>
					</div>
					<div class="col-sm-6 padding-top-med">
						<h3><img src="images/product/teach.svg"/> TeachAR&trade;</h3>
						<p>
							<?php if($lang === 'ko') { ?>
							다양한 실제/실험 상황에서 올바른 정보를 시각적으로 제공하는 효율적인 훈련 도구. 교육비용/불량률 감소. 제품/서비스의 생산성과 품질 향상으로 고객 만족도를 높여 줍니다.
							<?php } else { ?>
							Visual training tool with actual settings. Reduced training cost and defect rate. Enhanced productivity and quality of product, service, and customer satisfaction.
							<?php } ?>
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<h2 class="padding-top-med">PlayAR&trade;</h2>
	<p>
		<?php if($lang === 'ko') { ?>
		위치기반 엔터테인먼트 AR을 위한 검증된 솔루션
		<?php } else { ?>
		PlayAR&trade; is a proven solution for Location-Based Entertainment AR.
		<?php } ?>
	</p>
	<div class="product-brief-container" style="display:none;">
		<div class="column-2-3 column-2-3-img-container">
			<img src="images/product.png" width="100%"/>
		</div>

		<div class="column-1-3">
			<div class="text-left padding-left-small mobile-padding-top">
				<div class="img-leading">
					<img class="float-left" src="images/tablet.png"/>
					<h2 class="float-left small-margin">View AR</h2>
				</div>
				<p class="padding-img margin-top-10 margin-bottom-20">
					<?php if($lang === 'ko') { ?>
					기계 위에 상세 내역을 표시하고, 화면을 다른사람과 공유할 수 있습니다.
					<?php } else { ?>
					AR assists operations to provide a significant ROI in an assembly of complex devices that are performed by workers.
					<?php } ?>
				</p>
				<div class="img-leading">
					<img class="float-left" src="images/focus.png"/>
					<h2 class="float-left small-margin">Locate AR</h2>

				</div>
				<p class="padding-img margin-top-10 margin-bottom-20">
					<?php if($lang === 'ko') { ?>
					산업용 지도- 작업자 위치 및 전반적인 지도를 제공합니다.
					<?php } else { ?>
					With VPS(Visual Positioning System), workflow and trajectory can be optimized.
					<?php } ?>
				</p>
				<div class="img-leading">
					<img class="float-left" src="images/networking.png"/>
					<h2 class="float-left small-margin">Mapper AR</h2>

				</div>
				<p class="padding-img margin-top-10 margin-bottom-20">
					<?php if($lang === 'ko') { ?>
					공장 환경, 3D 모델로 만드는 모듈을 증강합니다.
					<?php } else { ?>
					Augument factory Environment and Modules Made with 3D Models
					<?php } ?>
				</p>
			</div>
		</div>
	</div>
</div>
<div class="padding-large text-center background-image" style="background-image:url(images/2.png)">
	<div class="small-gray-title"><?php if($lang === 'ko') echo 'WorkAR&trade;은 고객이 원하는 가치를 비약적으로 높여 드립니다.'; else echo 'WorkAR&trade; powerfully magnifies the value created';?></div>
	<div class="container padding-top-med">
		<div class="row">
			<div class="col-sm-8 col-centered">
				<?php if($lang === 'ko'){?>
				<div class="text-20 text-left">
					<ul class="space-list">
						<li>
							<span class="xx-large">V</span>isualize: AR은 불명확한 지시를 명확하고 직관적으로 바꿔 보여줍니다. 예를 들어 WorkAR&trade;은 대상물의 내외부 정보를 다양한 형태로 3차원 가시화해주고 어떻게 연결되는지를 알려줄수 있습니다.
						</li>
						<li>
							<span class="xx-large">A</span>ccess: 원거리에서도 원격 보조 시스템을 통해 WorkAR&trade;에 접속할 수 있습니다.
						</li>
						<li>
							<span class="xx-large">L</span>earn : 상품조립, 기계작동, 물류처리 같은 작업에 대해 순차적인 실시간 시각가이드를 제공하여 현장에서 바로 교육을 할 수 있습니다.
						</li>
						<li>
							<span class="xx-large">U</span>ser: AR은 사용자인터페이스의 완전히 새로운 경지를 열어갑니다. WorkAR&trade;은 어떤 상황에 대해서도 인터랙티브 AR 경험을 편리하게 제공합니다.
						</li>
						<li>
							<span class="xx-large">E</span>valuate: AR시스템은 카메라와 마이크를 사용하기 때문에 작업자의 상황을 쉽게 기록할 수 있어서, 디지털 작업일지 역할을 하며 작업자의 근무 효율과 성과를 평가할 수 있습니다.
						</li>
					</ul>
				</div>
				<?php }else{?>
				<div class="text-20 text-left">
					<ul class="space-list">
						<li>
							<span class="xx-large">V</span>isualize 3-D representations of the unit's internal or external information in multiple configurations and how subsystems fit together.
						</li>
						<li>
							<span class="xx-large">A</span>ccess WorkAR from long-distance through remote assistant system.
						</li>
						<li>
							<span class="xx-large">L</span>earn from real-time, onsite, step by step visual guide on tasks such as product assembly, machine operation, and warehouse picking.
						</li>
						<li>
							<span class="xx-large">U</span>ser friendly interface allows interactive AR experience in any setting.
						</li>
						<li>
							<span class="xx-large">E</span>valuate a worker in a cost-effective way through cameras, microphones.
						</li>
					</ul>
				</div>
				<?php }?>
			</div>
		</div>
	</div>
</div>
<div class="section-application text-center menu-linked-section" id="application-section">
	<div id="application-section-anchor" class="anchor-element"></div>
	<h2><?php if($lang === 'ko'){?>
		적용분야
		<?php }else{ ?>
		APPLICATIONS
		<?php } ?>
	</h2>
	<div class="container">
		<div class="row">
			<div class="col-sm-4 padding-top-med">
				<img src="images/factory-operation.svg"/>
				<h4 class="light-blue"><?php if($lang === 'ko') echo '공정 운영 관리'; else echo 'Factory oepration control and management';?></h4>
			</div>
			<div class="col-sm-4 padding-top-med">
				<img src="images/inspection-quality.svg"/>
				<h4 class="light-blue"><?php if($lang === 'ko') echo '검사/품질 관리'; else echo 'Inspection / Quality Control';?></h4>
			</div>
			<div class="col-sm-4 padding-top-med">
				<img src="images/remote-assistance.svg"/>
				<h4 class="light-blue"><?php if($lang === 'ko') echo '원격 보조'; else echo 'Remote assistance';?></h4>
			</div>
			<div class="col-sm-8 col-centered">
				<div class="row">
					<div class="col-sm-6 padding-top-med">
						<img src="images/smart-commerce.svg"/>
						<h4 class="light-blue"><?php if($lang === 'ko') echo '스마트커머스'; else echo 'Smart commerce';?></h4>
					</div>
					<div class="col-sm-6 padding-top-med">
						<img src="images/location-based.svg"/>
						<h4 class="light-blue"><?php if($lang === 'ko') echo '위치기반 엔터테인먼트'; else echo 'Location-based entertainment';?></h4>
					</div>
				</div>
			</div>
		</div>
	</div>
	
</div>
<div class="section-about menu-linked-section" id="company-section">
	<div id="company-section-anchor" class="anchor-element"></div>
	<div class="darken">
	</div>
	<div class="section-title">COMPANY</div>
	<div class="about-outter">
		<div class="about-inner">
			<h3 style="text-decoration: underline;">ABC AR: AN AUGMENTED REALITY COMPANY</h3>
			<p>
				<?php if($lang === 'ko') { ?>
				아톰앤비트는 원자(Atom)와 비트(Bit)를 합한 이름으로, 실세계와 디지털세계를 지능적으로 연결하는 AR 서비스를 개척하여, 디지털 정보가 널리 인간을 이롭게하는데 이바지하는 것을 목표로 합니다. 우리는 지난 25년 이상 선도한 AR 기술력과 노하우를 바탕으로, 산업, 엔터테인먼트, 교육, 의료 분야에서 세계 최고의 제품 및 서비스를 제공하고 있습니다.
				<?php } else { ?>
				Imagine if everyone in your organization could, at any moment, access the expert guidance they need to perform even the most complex tasks with ease. Imagine if that guidance was highly intuitive, accessible on any device, and easily updated. And imagine if each individual’s performance could be observed, measured, and improved. Scope AR makes all this a reality – today.
				<?php } ?>
			</p>
		</div>
	</div>
</div>
<div class="footer-container">
	<h3>Atom&Bit Co.</h3>
	<div class="footer-company-info-container">
		<p>222 Wangsimni-ro Suite #304<br>Songdong-gu<br>SEOUL, SOUTH KOREA</p>
		<p>hello@atom-bit.net</p>
		<p>@2019 by ABV Work AR</p>
	</div>
</div>
</body>
</html>